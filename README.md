# CarCar

Team:

* Brian Vazquez - Automobile Service
* Anton Zaitsev - Sales

## Description

This application is built for use of car dealerships. Built around three microservices. The inventory microservice is where vehicle models and manufacturers can be registered. Automobiles can then be added based on the manufacturers and models registered in the inventory. Automobiles represent vehicles that are in the dealership inventory that can be tracked based on their VIN. Then we have the Sales and Service microservices. The functionality for those are described below.

## Service microservice
In the service microservice I have built out 3 models that encompass the bounded context. An appointment model that has the fields; reason, customer name, datetime, automobile vin, technician(foriegn key), finished, and vip. the vip field turns to True if it matches to any vin of an automobile in our inventory that is stored in the AutomobileVO through polling. It remains false if vin is not in our inventory. The finished field turns to yes when a button is clicked on the react front end. Allowing it to be removed from the appointment list but remain in the database. The technician field gets matched to a technician object from the technician model. The technician model has two fields; name and employee number. Lastly we have AutombileVO which contains the field vin and href. The objects for this model are obtained through the polling of the inventory api.


## Sales microservice

For the sales microservice, the bounded context are all of the associated models contained within the service itself. That consists of an Automobile value object, that polls for data from the Inventory monolith, a Salesperson model, which contains a name field and an employee ID field, a Customer model, which contains a name, email, and phone number fields, and a Sales Record model, which contains foreign keys to the Customer name, Salesperson name, the vin of the Automobile value object, and a price field. There is an API call to display a sales history page, whose results can be filtered by the name of the salesperson. This directly feeds in data from a list of salespeople API call, and the general API call to display a list of sales records. Additionally, a new sale can be recorded via a sale submission form, which takes in a salesperson name, customer name, the VIN of the sold car, and the sale price of the car. Filtering for the sale history by salesperon name is done entirely on the front end through React.


## How to run
Clone this repository into your local computer and run docker-compose build then docker-compose up.
