from django.urls import path
from .views import list_technician, list_appointments,appointment_details


urlpatterns = [
    path("technicians/", list_technician, name="list_technician"),
    path("<int:id>/appointments/", appointment_details, name="appointment_details"),
    path("appointments/", list_appointments, name="list_appointments"),
]
