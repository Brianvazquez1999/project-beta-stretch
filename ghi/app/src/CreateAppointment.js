import { useState, useEffect} from 'react'

function CreateAppointment() {
const [technicians, setTechnicians] = useState("")
const [formData, setForm] = useState({
    customer_name: "",
    reason: "",
    date_time: "",
    automobile_vin:"",
    technician:"",
})
const fetchData = async () => {
const url = 'http://localhost:8080/service/technicians/'
const response = await fetch(url)
if (response.ok) {
    const data = await response.json()
    setTechnicians(data.technicians)
}
    }
    useEffect(() => {
        fetchData()
    }, [])




    const handleSubmit = async (event) => {
        event.preventDefault();
        const newList = technicians.filter((technician) => technician.employee_number !== formData.technician)
        setTechnicians(newList)
        const url = 'http://localhost:8080/service/appointments/'
        const fetchConfig = {
            method:"post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setForm({
                customer_name: "",
                reason: "",
                date_time: "",
                automobile_vin:"",
                technician:"",
            })
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setForm({
            ...formData, [inputName]: value
        })
    }


    return (
        <>
        { technicians && <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Make an Appointment</h1>
            <form onSubmit={handleSubmit} id="create-vehicle-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.customer_name} placeholder="customer_name" required type="text" name="customer_name" id="customer_name" className="form-control" />
                <label htmlFor="customer_name">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" />
                <label htmlFor="reason">Reason</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.date_time} placeholder="date_time" required type="datetime-local" name="date_time" id="date_time" className="form-control" />
                <label htmlFor="date_time">Enter Appointment Time</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.automobile_vin} placeholder="automobile_vin" required type="text" name="automobile_vin" id="automobile_vin" className="form-control" />
                <label htmlFor="automobile_vin">VIN</label>
              </div>
              <div className="mb-3">
                <select onChange={handleFormChange} required value={formData.technician} name="technician" id="technician" className="form-select">
                  <option value="">Choose a Technician</option>
                  {technicians.filter(technician => technician.reserved === "no").map(technician => {
                    return (
                      <option  key={technician.employee_number} value={technician.employee_number}>{technician.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Reserve</button>
            </form>
          </div>
        </div>
      </div>
}
</>
    )

}

export default CreateAppointment
