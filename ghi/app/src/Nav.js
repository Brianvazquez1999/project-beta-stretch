import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <div class="nav-item dropdown px-5 my-auto ">
  <button class="btn bg-success text-light dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
    Manufacturers
  </button>
  <ul class="dropdown-menu">
  <li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/manufacturers">Manufacturers List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/manufacturers/new">Add a Manufacturer</NavLink>
            </li>
  </ul>
</div>
<div class="nav-item dropdown px-5 my-auto">
  <button class="btn bg-success text-light dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
    Vehicle Models
  </button>
  <ul class="dropdown-menu">
  <li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/models">Vehicle Models List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/models/new">Add a New Vehicle Model</NavLink>
            </li>
  </ul>
</div>
<div class="nav-item dropdown my-auto px-5">
  <button class="btn bg-success text-light dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
    Automobiles
  </button>
  <ul class="dropdown-menu">
  <li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/automobiles">Automobiles List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/automobiles/new">Add a New Automobile</NavLink>
            </li>
  </ul>
</div>

<div class="nav-item dropdown my-auto px-5">
  <button class="btn bg-success text-light dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
    Service
  </button>
  <ul class="dropdown-menu">
  <li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/services/appointments">Active Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/services/appointments/new">Make an Appointment</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link active text-dark" to="/services/appointments/history">Service History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/technicians/new">Add a technician</NavLink>
              </li>
  </ul>
</div>
<div class="nav-item dropdown my-auto px-5">
  <button class="btn bg-success text-light dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
    Sales
  </button>
  <ul class="dropdown-menu">
<li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/sales/list">List of Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/sales/new">Record a new sale</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/sales/history">Sale person history</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/salespeople/new">Add a Sales Person</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link active text-dark" to="/customer/new">Add a Customer</NavLink>
              </li>
  </ul>
</div>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
