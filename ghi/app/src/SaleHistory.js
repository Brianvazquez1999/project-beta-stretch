import { Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

const SaleHistory = () => {
    const [sales, setSales] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [filterTerm, setFilterTerm] = useState("")

    const getData = async () => {
        const resp = await fetch('http://localhost:8090/api/salesrecords/')
        if (resp.ok) {
            const data = await resp.json()
            const sales = data.records.map((sale) => {
                return {
                  salespersonName: sale.salesperson.name,
                  customerName: sale.customer.name,
                  vin: sale.automobile.vin,
                  price: sale.price
                };
              });
            setSales(sales)
        }

        const salesperson_resp = await fetch('http://localhost:8090/api/salespeople/')
        if (salesperson_resp.ok) {
            const data = await salesperson_resp.json()
            const salespeople = data.salespeople.map((salesperson) => {
                return {
                  salespersonName: salesperson.name,
                };
              });
              setSalespeople(salespeople)
        }

    }

    useEffect(() => {
        getData()
    }, [])

    const handleFilterChange = (e) => {
        setFilterTerm(e.target.value);
    }

    return <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Sales person history</h1>
                    <select onChange={handleFilterChange} name="salesperson" id="salesperson" className="form-control">
                                <option value="">Choose a salesperson</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.salespersonName} value={salesperson.salespersonName}>
                                            {salesperson.salespersonName}
                                        </option>
                                    );
                                })}
                            </select>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Sales person</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Sale price</th>
                            </tr>
                        </thead>
                        <tbody>
                        {sales
                            .filter((sale) => sale.salespersonName.includes(filterTerm))
                            .map((filteredSale) => {
                            return (
                                <tr>
                                    <td>{filteredSale.salespersonName}</td>
                                    <td>{filteredSale.customerName}</td>
                                    <td>{filteredSale.vin}</td>
                                    <td>{filteredSale.price}</td>
                                </tr>
                            );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </>
}

export default SaleHistory;
