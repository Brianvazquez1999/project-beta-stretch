import { Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

function ListSales () {
    const [sales, setSales] = useState([])

    const getData = async () => {
        const resp = await fetch('http://localhost:8090/api/salesrecords/')
        if (resp.ok) {
            const data = await resp.json()
            setSales(data.records)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Sale Records</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Sales Person</th>
                                <th>Employee Number</th>
                                <th>Customer Name</th>
                                <th>VIN</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                sales.map(sale => {
                                    return (
                                        <tr sale={sale.automobile.vin}>
                                            <td>{sale.salesperson.name}</td>
                                            <td>{sale.salesperson.emp_id}</td>
                                            <td>{sale.customer.name}</td>
                                            <td>{sale.automobile.vin}</td>
                                            <td>{sale.price}</td>
                                        </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </>
}

export default ListSales;
