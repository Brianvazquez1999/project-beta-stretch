import React, {useState, useEffect} from 'react';

function SaleForm() {
    const [listAutoSuccess, setListAutoSuccess] = useState(false)
    const [listAutoSoldSuccess, setListAutoSoldSuccess] = useState(false)
    const [filteredVins, setFilteredVins] = useState([])
    const [autos, setAutos] = useState([])
    const [soldVins, setSoldVins] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [unfilteredVins, setUnfilteredVins] = useState([])
    const [salesRecords, setSalesRecords] = useState([])
    const [customers, setCustomers] = useState([])
    const [formData, setFormData] = useState({
        automobile: '',
        salesperson: '',
        customer: '',
        price: '',
    })


    const getData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url)

        if(response.ok) {
            const data = await response.json()
            const autoData = data.autos
            setAutos(autoData);

            const vins = []
            for (let i = 0; i < autoData.length; i++) {
                const auto = autoData[i]
                const vin = auto.vin
                vins.push(vin)
            }
            setUnfilteredVins(vins)
            setListAutoSuccess(true)
        }

        const salespeople_url = 'http://localhost:8090/api/salespeople/';
        const salespeople_response = await fetch(salespeople_url)

        if(response.ok) {
            const data = await salespeople_response.json()
            setSalespeople(data.salespeople);
        }

        const customers_url = 'http://localhost:8090/api/customers/';
        const customers_response = await fetch(customers_url)

        if(response.ok) {
            const data = await customers_response.json()
            setCustomers(data.customers);
        }

        const salesRecords_url = 'http://localhost:8090/api/salesrecords/';
        const salesRecords_response = await fetch(salesRecords_url)

        if(response.ok) {
            const data = await salesRecords_response.json()
            const recordData = data.records
            setSalesRecords(recordData);

            const soldVins = []
            for (let i = 0; i < recordData.length; i++) {
                const record = recordData[i]
                const vin = record.automobile.vin
                soldVins.push(vin)
            }
            setSoldVins(soldVins)
            setListAutoSoldSuccess(true)
        }
    }

    useEffect(() => {
        if (!(listAutoSoldSuccess && listAutoSuccess)) {
            getData();
        } else {
            const filteredVinsList = []
            for (let i = 0; i < unfilteredVins.length; i++) {
                const unfilteredVin = unfilteredVins[i]
                if (!(soldVins.includes(unfilteredVin))) {
                    filteredVinsList.push(unfilteredVin)
                }
            }
            setFilteredVins(filteredVinsList)
        }
    }, [listAutoSoldSuccess, listAutoSuccess])

    const handleSubmit = async (event) => {
            event.preventDefault();

            const saleUrl = `http://localhost:8090/api/salesrecords/`;

            const fetchConfig = {
                method: "post",
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

        const response = await fetch(saleUrl, fetchConfig)

        if (response.ok) {
            setFormData({
                automobile: '',
                salesperson: '',
                customer: '',
                price: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} name="automobile" id="automobile" className="form-control">
                                <option value={formData.automobile}>Choose an automobile</option>
                                {autos.filter(auto => {
                                    return (filteredVins.includes(auto.vin))
                                }).map(auto => {
                                    return (
                                        <option key={auto.vin} value={auto.vin}>
                                        {auto.model.manufacturer.name} {auto.model.name} ({auto.vin})
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} name="salesperson" id="salesperson" className="form-control">
                                <option value={formData.salesperson}>Choose a sales person</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.id} value={salesperson.id}>
                                        {salesperson.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} name="customer" id="customer" className="form-control">
                                <option value={formData.customer}>Choose a customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>
                                        {customer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}



export default SaleForm;
