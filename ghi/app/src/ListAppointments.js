import { useState, useEffect} from 'react'
import moment from 'moment'
function AppointmentList () {
    const [appointments, setAppointments] = useState(null)

    const getData = async () => {
        const resp = await fetch('http://localhost:8080/service/appointments/')
        if (resp.ok) {
            const data = await resp.json()
            setAppointments(data.appointments)

        }
    }



    useEffect(() => {
        getData()

    },[])



    async function handleFinished(id){
        const newList = appointments.filter((appointment) => appointment.id !== id )
        const url = `http://localhost:8080/service/${id}/appointments/`
        for (const appointment of appointments) {
            if (appointment.id === id) {
                appointment.finished = "yes"
                appointment.technician.reserved = "no"
                console.log(appointment.technician.reserved)
            }
        }

        const fetchConfig = {
            method:"put",
            body: JSON.stringify(appointments.filter((appointment) => appointment.id === id)),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setAppointments(newList)
        }


    }
    async function handleCancel(id) {
        const newList = appointments.filter((appointment) => appointment.id !== id)
        for (const appointment of appointments) {
            if(appointment.id === id) {
                appointment.technician.reserved = "no"
            }
        }


        const url = `http://localhost:8080/service/${id}/appointments/`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const resp = await fetch(url, fetchConfigs)
        if (resp.ok) {
            setAppointments(newList)
        }
    }



    return ( <>
    {appointments && <div className="offset-3 col-6">
<div className="row">
                <div className="shadow p-4 mt-4">
                    <h1>Service Appointments</h1>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Vin</th>
                                <th>Customer name</th>
                                <th>Date and Time</th>
                                <th>Reason</th>
                                <th>Technician</th>
                                <th>VIP?</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                appointments.filter((appointment) => appointment.finished === "no").map(appointment => {
                                    const value = appointment.vip
                                    return (

                                        <>
                                        <tr key={appointment.id}>
                                            <td>{appointment.automobile_vin}</td>
                                            <td>{appointment.customer_name}</td>
                                            <td>{moment(appointment.date_time).format("MMMM Do YYYY, h:mm a")}</td>
                                            <td>{appointment.reason}</td>
                                            <td>{appointment.technician.name}</td>
                                            <td>{String(value)}</td>
                                            <div>
                                         <button value={appointment.finished} onClick={() => handleFinished(appointment.id)}type="button" className="btn btn-primary">Finished</button>
                                        <button onClick={() => handleCancel(appointment.id)}type="button" className="btn btn-danger">Cancel</button>
                                            </div></tr>
                                        </>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>}</>
    )
}

export default AppointmentList;
