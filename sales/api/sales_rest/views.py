from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import AutomobileVOEncoder, SalespeopleEncoder, CustomerEncoder, SalesRecordEncoder
from .models import AutomobileVO, Customer, Salesperson, SalesRecord


@require_http_methods(["GET"])
def api_check_auto_vo(request):

    if request.method == "GET":

        auto = AutomobileVO.objects.all()
        return JsonResponse(
            {"auto": auto},
            encoder=AutomobileVOEncoder,
        )


@require_http_methods(["GET", "POST"])
def api_salespeople(request):

    if request.method == "GET":

        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespeopleEncoder,
        )

    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespeopleEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":

        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )

    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_sales_records(request):
    if request.method == "GET":

        records = SalesRecord.objects.all()
        return JsonResponse(
            {"records": records},
            encoder=SalesRecordEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            salesperson = Salesperson.objects.get(name=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist"},
                status=400,
            )

        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin"},
                status=400,
            )

        try:
            customer = Customer.objects.get(name=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )


        record = SalesRecord.objects.create(**content)
        return JsonResponse(
            record,
            encoder=SalesRecordEncoder,
            safe=False,
        )
