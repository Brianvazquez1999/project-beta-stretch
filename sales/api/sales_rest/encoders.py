from common.json import ModelEncoder
from .models import AutomobileVO, Customer, Salesperson, SalesRecord

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "year",
        "color",
        "model",
    ]

class SalespeopleEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "name",
        "emp_id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
    ]

class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "automobile",
        "customer",
        "salesperson",
        "price",
    ]

    encoders = {
        "salesperson": SalespeopleEncoder(),
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerEncoder(),
    }
